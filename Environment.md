# 集群说明
本次测试使用3个节点构成的集群。
| HostName | IP地址 |
| - | - |
| master | 10.1.11.140 |
| node0 | 10.1.11.141 |
| node1 | 10.1.11.142 |

三台主机的物理网卡处于同一网段下，可以直接相互通信。

# 系统环境
```
操作系统：CentOS-7
发行版本：CentOS Linux release 7.9.2009 (Core)
内核版本：Linux version 3.10.0-957.el7.x86_64
```

# Go
```
# go version
go version go1.15.5 linux/amd64
```

# Docker
```
# docker version
Client:
 Version:         1.13.1
 API version:     1.26
 Package version: docker-1.13.1-208.git7d71120.el7_9.x86_64
 Go version:      go1.10.3
 Git commit:      7d71120/1.13.1
 Built:           Mon Jun  7 15:36:09 2021
 OS/Arch:         linux/amd64

Server:
 Version:         1.13.1
 API version:     1.26 (minimum version 1.12)
 Package version: docker-1.13.1-208.git7d71120.el7_9.x86_64
 Go version:      go1.10.3
 Git commit:      7d71120/1.13.1
 Built:           Mon Jun  7 15:36:09 2021
 OS/Arch:         linux/amd64
 Experimental:    false
```

# OpenYurt
OpenYurt Version: 0.4.0  
Commit: 2abb8ad