# 一致性测试过程

使用的一致性测试工具: sonobuoy
```
# sonobuoy version
Sonobuoy Version: v0.52.0
MinimumKubeVersion: 1.17.0
MaximumKubeVersion: 1.99.99
GitSHA: 3da67b690a5aca93860dcbf5040e468abe183266
```    

以下一致性测试过程参考：
[cncf/k8s-conformance/instructions.md](https://github.com/cncf/k8s-conformance/blob/master/instructions.md)
commit: 307bac3


## 下载sonobuoy
```
# mkdir $HOME/sonobuoy && cd $HOME/sonobuoy
# wget --no-check-certificate https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.52.0/sonobuoy_0.52.0_linux_amd64.tar.gz
# tar -zxvf sonobuoy_0.52.0_linux_amd64.tar.gz
# cp sonobuoy /usr/bin
```

## 进行一致性测试
在该步骤中，我们使用sonobuoy提供的e2e插件，并在OpenYurt上运行其中带有`[conformance]`标记的测试。
```
# sonobuoy run --mode=certified-conformance
```

## 查看测试状态
```
# sonobuoy status
         PLUGIN     STATUS   RESULT   COUNT
            e2e   complete   passed       1
   systemd-logs   complete   passed       3

Sonobuoy has completed. Use `sonobuoy retrieve` to get results.
```

## 获取测试结果
```
# cd $HOME/sonobuoy
# sonobuoy results $(sonobuoy retrieve)
Plugin: e2e
Status: passed
Total: 4992
Passed: 277
Failed: 0
Skipped: 4715

Plugin: systemd-logs
Status: passed
Total: 3
Passed: 3
Failed: 0
Skipped: 0
```
各个kubernetes版本的测试结果（即sonobuoy retrieve的输出文件）保存在对应的文件夹下，文件名为（sonobuoy-results.tar.gz）。在对应目录下使用命令 
```
# sonobuoy results sonobuoy-results.tar.gz
``` 
可以查看测试结果。