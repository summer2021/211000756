#!/bin/bash

# Set LOGINPASSWORD, LOGINUSERNAME, MASTERIPADDR manually
# This script will fetch files from ${LOGINUSERNAME}@${MASTERIPADDR}
# with password ${LOGINPASSWORD}

set -x
set -e errexit
set -u

# Setup YurtHub
mkdir -p /etc/kubernetes/manifests
sshpass -p "$LOGINPASSWORD" scp "$LOGINUSERNAME"@"$MASTERIPADDR":/tmp/yurthub-ack.yaml /etc/kubernetes/manifests

mkdir -p /var/lib/openyurt
cp /etc/kubernetes/kubelet.conf /var/lib/openyurt

sed -i '/certificate-authority-data/d;
    /client-key/d;
    /client-certificate/d;
    /user:/d;
    s/ https.*/ http:\/\/127.0.0.1:10261/g' /var/lib/openyurt/kubelet.conf

cp /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf ./10-kubeadm.conf.backup

sed -i "s|KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=\/etc\/kubernetes\/bootstrap-kubelet.conf\ --kubeconfig=\/etc\/kubernetes\/kubelet.conf|KUBELET_KUBECONFIG_ARGS=--kubeconfig=\/var\/lib\/openyurt\/kubelet.conf|g" \
    /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf


