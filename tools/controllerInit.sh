#!/bin/bash

# This script will 1)label and annotate k8s nodes,
# 2)modify the kube-controller-manager.yaml in /etc/kubernetes/manifests
# and then 3)setup the OpenYurt-Controller-Manager.
# Thus, it should run on the master node.

set -x
set -e errexit
set -u

cloudnodes=(master)
edgenodes=(node0 node1)
CLOUDNODES=(${CLOUDNODES[@]:-${cloudnodes[@]}})
EDGENODES=(${EDGENODES[@]:-${edgenodes[@]}})
KUBECONTROLLERDIR="/etc/kubernetes/manifests"
OPENYURTPATH=${OPENYURTPATH:-${HOME}/openyurt}

for i in "${CLOUDNODES[@]}"; do
    echo "label cloud node $i"
    kubectl label node "$i" openyurt.io/is-edge-worker=false --overwrite
done

for i in "${EDGENODES[@]}"; do
    echo "label edge node $i"
    kubectl label node "$i" openyurt.io/is-edge-worker=true --overwrite
    echo "annotate edge node $i"
    kubectl annotate node "$i" node.beta.alibabacloud.com/autonomy=true --overwrite
done

# disable the nodelifecycle controller
if [ ! -f ${OPENYURTPATH}/kube-controller-manager.yaml.backup ]; then
    echo "make the backup of the original kube-controller-manager.yaml"
    cp ${KUBECONTROLLERDIR}/kube-controller-manager.yaml ${OPENYURTPATH}/kube-controller-manager.yaml.backup
fi

if [ -n $(sed -n /^.*--controllers.*-nodelifecycle.*/p $KUBECONTROLLERDIR/kube-controller-manager.yaml) ]; then
    echo "disable the native nodelifecycle controller"
    sed -i 's/^.*--controllers.*/&,-nodelifecycle/' $KUBECONTROLLERDIR/kube-controller-manager.yaml
fi

# Use specific image version instead of 'latest'
cat ${OPENYURTPATH}/config/setup/yurt-controller-manager.yaml | sed 's/image: openyurt\/yurt-controller-manager:latest/image: openyurt\/yurt-controller-manager:v0.4.0/' \
    > ${OPENYURTPATH}/config/setup/yurt-controller-manager-v0.4.0.yaml

kubectl apply -f $OPENYURTPATH/config/setup/yurt-controller-manager-v0.4.0.yaml

# Prepare YurtHub yaml for edge nodes
cat ${OPENYURTPATH}/config/setup/yurthub.yaml |
        sed 's|__pki_path__|/etc/kubernetes/pki|;
        s|__kubernetes_service_host__|10.1.11.140|;
        s|__kubernetes_service_port_https__|6443|;
        s|image: openyurt/yurthub:latest|image: openyurt/yurthub:v0.4.0|;
        s|imagePullPolicy: Always|imagePullPolicy: IfNotPresent|' > /tmp/yurthub-ack.yaml
