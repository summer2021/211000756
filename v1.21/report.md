# k8s-v1.21测试报告

## 软件版本
OpenYurt: 0.4.0, commit: 2abb8ad  
Kubernetes: 1.21.3  
Sonobuoy: 0.52.0  

## 测试结果
一致性测试样例数：339  
样例通过数：339  
结果：通过  

## sonobuoy results
Plugin: e2e  
Status: passed  
Total: 5771  
Passed: 339  
Failed: 0  
Skipped: 5432  

Plugin: systemd-logs  
Status: passed  
Total: 3  
Passed: 3  
Failed: 0  
Skipped: 0  
