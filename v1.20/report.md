# k8s-v1.20测试报告

## 软件版本
OpenYurt: 0.4.0, commit: 2abb8ad  
Kubernetes: 1.20.1  
Sonobuoy: 0.52.0  

## 测试结果
一致性测试样例数：311  
样例通过数：311  
结果：通过  

## sonobuoy results
Plugin: e2e  
Status: passed  
Total: 5667  
Passed: 311  
Failed: 0  
Skipped: 5356  

Plugin: systemd-logs  
Status: passed  
Total: 3  
Passed: 3  
Failed: 0  
Skipped: 0  
