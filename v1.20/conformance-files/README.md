# Conformance Test Steps for OpenYurt

## STEP1: Label cloud nodes and edge nodes

We first need to divide nodes into two categories, the cloud node
and the edge node, by using label `openyurt.io/is-edge-worker`. Assume that the given Kubernetes cluster
has three nodes,
```bash
[root@master ~]# kubectl get node -A
NAME     STATUS   ROLES                  AGE    VERSION
master   Ready    control-plane,master   166m   v1.20.1
node0    Ready    <none>                 166m   v1.20.1
node1    Ready    <none>                 164m   v1.20.1
```
and we will use node `master` as the cloud node.

We label the cloud node with value `false`,
```bash
kubectl label node master openyurt.io/is-edge-worker=false
```

and edge nodes with value `true`.
```bash
kubectl label node node0 openyurt.io/is-edge-worker=true
kubectl label node node1 openyurt.io/is-edge-worker=true
```

Then we active the autonomous mode, annotating edge nodes by typing following command
```bash
kubectl annotate node node0 node.beta.alibabacloud.com/autonomy=true
kubectl annotate node node1 node.beta.alibabacloud.com/autonomy=true
```

## STEP2: Setup Yurt-controller-manager

Next, we need to deploy the Yurt controller manager, which prevents apiserver from evicting pods running on the
autonomous edge nodes during disconnection. Before apply, we need to modify the yaml to use specific image version instead the latest.
```bash
wget --no-check-certificat https://github.com/openyurtio/openyurt/blob/release-v0.4/config/setup/yurt-controller-manager.yaml
cat yurt-controller-manager.yaml | sed 's/image: openyurt\/yurt-controller-manager:latest/image: openyurt\/yurt-controller-manager:v0.4.0/' \
    > yurt-controller-manager-v0.4.0.yaml

kubectl apply -f yurt-controller-manager-v0.4.0.yaml
```

## STEP3: Disable the default nodelifecycle controller

To allow the yurt-controller-mamanger to work properly, we need to turn off the default nodelifecycle controller.  
We add `-nodelifecycle` to the `--controller` option of kube-controller-manager.

If the kube-controller-manager is deployed as a static pod on the master node, and you have the permission to login
to the master node, then above operations can be done by revising the file `/etc/kubernetes/manifests/kube-controller-manager.yaml`. 
```bash
# on master node
cp /etc/kubernetes/manifests/kube-controller-manager.yaml /etc/kubernetes/manifests/kube-controller-manager.yaml.backup
sed  's/^.*--controllers.*/&,-nodelifecycle/' /etc/kubernetes/manifests/kube-controller-manager.yaml \
     > /etc/kubernetes/manifests/kube-controller-manager.yaml
```

## STEP4: Setup Yurthub

After the Yurt controller manager is up and running, we will setup Yurthub as the static pod. **These commands should run on each of edge nodes.** Before proceeding,
please 
**get the apiserver's address (i.e., ip:port)**, which will be used to replace the place holder in the template
file `yurthub.yaml`. In the following command, we assume that the address of the apiserver is 1.2.3.4:5678
```bash
# on edge node
mkdir -p /etc/kubernets/manifests

wget --no-check-certificate https://github.com/openyurtio/openyurt/blob/release-v0.4/config/setup/yurthub.yaml
cat yurthub.yaml |
sed 's|__pki_path__|/etc/kubernetes/pki|;
s|__kubernetes_service_host__|1.2.3.4|;
s|__kubernetes_service_port_https__|5678|' > /etc/kubernetes/manifests/yurthub-ack.yaml
```
and the Yurthub will be ready in minutes.


Now we need to reset the
kubelet service to let it access the apiserver through the yurthub. As kubelet will connect to the Yurthub through http, so we create a new kubeconfig file for the kubelet service.
```bash
# on edge node
mkdir -p /var/lib/openyurt
cat << EOF > /var/lib/openyurt/kubelet.conf
apiVersion: v1
clusters:
- cluster:
    server: http://127.0.0.1:10261
  name: default-cluster
contexts:
- context:
    cluster: default-cluster
    namespace: default
    user: default-auth
  name: default-context
current-context: default-context
kind: Config
preferences: {}
EOF
```

In order to let kubelet to use the new kubeconfig, we edit the drop-in file of the kubelet
service (i.e., `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf`)
```bash
# on edge node
sed -i "s|KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=\/etc\/kubernetes\/bootstrap-kubelet.conf\ --kubeconfig=\/etc\/kubernetes\/kubelet.conf|KUBELET_KUBECONFIG_ARGS=--kubeconfig=\/var\/lib\/openyurt\/kubelet.conf|g" \
    /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
```

Finally, we restart the kubelet service
```bash
# on edge node
systemctl daemon-reload && systemctl restart kubelet
```

## STEP5: Start Conformance Test
Assuming that we've got the executable file `sonobuoy:v0.52.0`, then we run the cmd
```bash
sonobuoy run --mode=certified-conformance
```
to start the conformance test.

## STEP6: Fetch Test Result
We can use `sonobuoy status` to check the test running status. If we find all tests has finished, we can use the following command to get the result.
```bash
sonobuoy results $(sonobuoy retrieve)
```