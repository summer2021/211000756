# k8s-v1.19测试报告

## 软件版本
OpenYurt: 0.4.0, commit: 2abb8ad  
Kubernetes: 1.19.13  
Sonobuoy: 0.52.0  

## 测试结果
一致性测试样例数：305  
样例通过数：305  
结果：通过  

## sonobuoy results
Plugin: e2e  
Status: passed  
Total: 5484  
Passed: 305  
Failed: 0  
Skipped: 5179  

Plugin: systemd-logs  
Status: passed  
Total: 3  
Passed: 3  
Failed: 0  
Skipped: 0  