# Openyurt Conformance Test

## 文件说明

| 文件/目录 名称          | 说明                       |
| ----------------------- | -------------------------- |
| v1.18                   | Kubernetes v1.18的测试结果 |
| v1.19                   | Kubernetes v1.19的测试结果 |
| v1.20                   | Kubernetes v1.20的测试结果 |
| v1.21                   | Kubernetes v1.21的测试结果 |
| TestInstructions.md     | 一致性测试过程             |
| OpenYurtInstallation.md | OpenYurt安装过程           |
| Environment.md          | 测试环境                   |

在各个k8s版本目录下，有如下目录结构
```
├── conformance-files
│   ├── e2e.log
│   ├── junit_01.xml
│   └── README.md
├── report.md
└── sonobuoy-results.tar.gz
```
其中  
1. conformance-files目录下包含k8s一致性认证(cncf/k8s-conformance)所需文件。
2. report.md记录了当前版本的一致性测试结果
3. sonobuoy-results.tar.gz为sonobuoy一致性测试的原始结果（即通过`sonobuoy retrieve`获取的文件）


## 测试过程
1. 准备好[测试环境](./Environment.md)
2. 安装相应版本的Kubernetes
3. [安装OpenYurt](./OpenYurtInstallation.md)
4. [进行一致性测试](./TestInstructions.md)

## 测试结果
| k8s版本 | 测试结果 | 测试报告          |
| ------- | -------- | ----------------- |
| v1.18   | passed   | [v1.18](./v1.18/report.md) |
| v1.19   | passed   | [v1.19](./v1.19/report.md) |
| v1.20   | passed   | [v1.20](./v1.20/report.md) |
| v1.21   | passed   | [v1.21](./v1.21/report.md) |

## 完成进度
目前已将conformance-files合入openyurt中，存放在k8s-conformance中对应k8s版本的目录下。