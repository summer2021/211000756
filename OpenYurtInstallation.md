# OpenYurt安装
在安装OpenYurt前，我们需要一个正常运行的Kubernetes集群。

使用的OpenYurt版本：
```
OpenYurt: 0.4.0
Commit: 2abb8ad
```

OpenYurt安装过程参考：[Manually Setup](https://github.com/openyurtio/openyurt/blob/release-v0.4/docs/tutorial/manually-setup.md)


## yaml修改
在上述安装过程中会自动使用最新镜像，导致镜像与测试的Openyurt版本不匹配产生问题。因此需要修改setup时所用的yaml文件，修改后的文件放在setup目录下。  
更改的文件和字段为：  
[yurt-controller-manager.yaml](https://gitlab.summer-ospp.ac.cn/summer2021/211000756/-/blob/master/config/setup/yurt-controller-manager.yaml)  
Depolyment: yurt-controller-manager  
Path: /spec/containers/image  

[yurthub.yaml](https://gitlab.summer-ospp.ac.cn/summer2021/211000756/-/blob/master/config/setup/yurthub.yaml)  
Pod: yurt-hub  
Path: /spec/containers/image, /spec/containers/imagePullPolicy  
